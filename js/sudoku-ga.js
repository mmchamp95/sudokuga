(function () {
	// create app
	var sudoku_gaApp = angular.module('sudoku-gaApp', ['chart.js']);

	// set controller
	sudoku_gaApp.controller('mainCtrl', ['$scope', function ($scope) {
		// private members

		var genetic, initialBoard = [],
			settings = [
				[100, 200, 300], // size
				[0.1, 0.5], // crossover
				[0.01, 0.05], // mutation
				[100], // iterations
				[true/*, false*/], // fittest always survives
				['Tournament2'/*, 'Tournament3', 'Fittest', 'Random', 'RandomLinearRank', 'Sequential'*/], // select1
				['Tournament2'/*, 'Tournament3', 'Random', 'RandomLinearRank', 'Sequential', 'FittestRandom'*/], // select2
				['Uniform'/*, 'MultiPoint'*/] // crossover algorithm
			];

		// returns a random number between min and max
		function randRange(min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		// returns a clone of the passed array
		function clone2DArray(array) {
			var clone = [], i;

			for (i = 0; i < array.length; i++) {
				clone[i] = array[i].slice();
			}

			return clone;
		}

		// returns an array of all possible combinations of elements in each
		// array of passed array
		function cartesian(arg) {
			var result = [], max = arg.length - 1;

			// helper function used to determine cartesian product
			function helper(array, i) {
				for (var j = 0, l = arg[i].length; j < l; j++) {
					var a = array.slice(0);

					a.push(arg[i][j]);

					if (i === max) {
						result.push(a);
					} else {
						helper(a, i + 1);
					}
				}
			}
			helper([], 0);

			return result;
		}

		// runs a number of tests using different configurations
		function runTests(index) {
			var tests = cartesian(settings),
				test;

			if (index === undefined) {
				index = 0;
			}

			// get test settings
			test = tests[index];

			if (test !== undefined) {
				// set configuration
				$scope.popSize = test[0];
				$scope.crossover = test[1];
				$scope.mutation = test[2];
				$scope.iterations = test[3];
				$scope.fittestAlwaysSurvives = test[4];
				$scope.select1 = test[5];
				$scope.select2 = test[6];
				$scope.crossoverAlgorithm = test[7];

				// run test
				$scope.solve(function (index) {
					// append output
					$scope.output += '\n' + $scope.popSize + ', ' + $scope.iterations + ', ' + $scope.crossover + ', ' + $scope.mutation + ', ' + $scope.globalMax;
					$scope.$apply();

					// run next test
					runTests(index);
				}, index + 1);
			}
		}

		// checks a row for a duplicate value
		function checkCellRow(board, row, col) {
			var value = board[row][col].value, i;

			for (i = 0; i < board.length; i++) {
				if (i !== col && board[row][i].value === value) {
					return false;
				}
			}

			return true;
		}

		// checks a column for a duplicate value
		function checkCellCol(board, row, col) {
			var value = board[row][col].value, i;

			for (i = 0; i < board.length; i++) {
				if (i !== row && board[i][col].value === value) {
					return false;
				}
			}

			return true;
		}

		// checks a box for a duplicate value
		function checkCellBox(board, row, col) {
			var value = board[row][col].value,
				boxSize = Math.sqrt(board.length),
				rStart = Math.floor(row / boxSize) * boxSize,
				rEnd = rStart + boxSize,
				cStart = Math.floor(col / boxSize) * boxSize,
				cEnd = cStart + boxSize,
				r,
				c;

			for (r = rStart; r < rEnd; r++) {
				for (c = cStart; c < cEnd; c++) {
					if (!(r === row && c === col) && board[r][c].value === value) {
						return false;
					}
				}
			}

			return true;
		}

		// returns if a row is valid, passing a value is optional
		function checkRow(board, row, value) {
			var map = {}, invalids = 0, i;

			if (value !== undefined) {
				map[value] = true;
			}

			for (i = 0; i < board.length; i++) {
				if (board[row][i].value !== undefined) {
					if (map[board[row][i].value]) {
						return false;
					}
					map[board[row][i].value] = true;
				}
			}

			return true;
		}

		// returns if a column is valid, passing a value is optional
		function checkCol(board, col, value) {
			var map = {}, i;

			if (value !== undefined) {
				map[value] = true;
			}

			for (i = 0; i < board.length; i++) {
				if (board[i][col].value !== undefined) {
					if (map[board[i][col].value]) {
						return false;
					}
					map[board[i][col].value] = true;
				}
			}

			return true;
		}

		// returns if a box is valid, passing a value is optional
		function checkBox(board, row, col, value) {
			var map = {},
				boxSize = Math.sqrt(board.length),
				rStart = Math.floor(row / boxSize) * boxSize,
				rEnd = rStart + boxSize,
				cStart = Math.floor(col / boxSize) * boxSize,
				cEnd = cStart + boxSize,
				r,
				c;

			if (value !== undefined) {
				map[value] = true;
			}

			for (r = rStart; r < rEnd; r++) {
				for (c = cStart; c < cEnd; c++) {
					if (board[r][c].value !== undefined) {
						if (map[board[r][c].value]) {
							return false;
						}
						map[board[r][c].value] = true;
					}
				}
			}

			return true;
		}

		// returns a new sudoku puzzle
		function newInitialBoard() {
			var sudoku = new Sudoku(),
				size,
				board,
				val,
				i, j, k;

			// generate puzzle
			sudoku.newGame();
			size = Math.sqrt(sudoku.matrix.length);

			// initialize board
			for (i = 0; i < size; i++) {
				initialBoard[i] = [];
				for (j = 0; j < size; j++) {
					initialBoard[i][j] = {stuck: false};
				}
			}

			// populate board with stuck values
			for (i = 0; i < sudoku.matrix.length; i++) {
				val = sudoku.matrix[i];
				if (val !== 0) {
					j = Math.floor(i / size);
					k = i % size;
					initialBoard[j][k].value = val;
					initialBoard[j][k].stuck = true;
				}
			}

			// set board in scope
			$scope.board = clone2DArray(initialBoard);
		}

		// initializes controller
		function init() {
			// create initial board
			newInitialBoard();

			// set default genetic algorithm options
			$scope.popSize = 100;
			$scope.crossover = 0.5;
			$scope.mutation = 0.25;
			$scope.iterations = 1000;
			$scope.skip = 100;
			$scope.fittestAlwaysSurvives = false;
			$scope.select1 = 'Tournament2';
			$scope.select2 = 'Tournament2';
			$scope.crossoverAlgorithm = 'MultiPoint';

			// set statistical variables
			$scope.isFinished = true;
			$scope.generation = 0;
			$scope.stats = {
				maximum: 0,
				minimum: 0,
				mean: 0,
				stdev: 0
			};
			$scope.globalMax = 0;
			$scope.labels = [];
			$scope.series = ['Best', 'Average', 'Worst', 'Std. Deviation'];
			$scope.data = [[], [], [], []];

			// set chart settings
			Chart.defaults.global.animation = false;
			Chart.defaults.global.responsive = true;
		}

		// public methods

		// creates a new board
		$scope.new = function () {
			newInitialBoard();
		};

		// runs varius tests
		$scope.test = function () {
			$scope.output = 'Population, Generations, Crossover, Mutation, Result';
			runTests();
		};

		// solves the board using a genetic algorithm
		$scope.solve = function (callback, index) {
			// reset board
			if ($scope.generation !== 0) {
				$scope.board = clone2DArray(initialBoard);
				$scope.labels = [];
				$scope.series = ['Best', 'Average', 'Worst', 'Std. Deviation'];
				$scope.data = [[], [], [], []];
				$scope.globalMax = 0;
			}

			// genetic algorithm configuration
			var config = {
				'iterations': $scope.iterations,
				'size': $scope.popSize,
				'crossover': 1,
				'mutation': $scope.mutation,
				'skip': $scope.skip,
				'fittestAlwaysSurvives': $scope.fittestAlwaysSurvives
			},

			// helper functions to be used within genetic algorithm workspace
			userData = {
				initialBoard: initialBoard,
				randRange: randRange,
				clone2DArray: clone2DArray,
				checkRow: checkRow,
				checkCol: checkCol,
				checkBox: checkBox,
				checkCellRow: checkCellRow,
				checkCellCol: checkCellCol,
				checkCellBox: checkCellBox,
				crossoverAlgorithm: $scope.crossoverAlgorithm,
				crossover: $scope.crossover
			};

			// initialize genetic algorithm
			genetic = Genetic.create();

			// set optimization type
			genetic.optimize = Genetic.Optimize.Maximize;

			// selects individuals for survival
			genetic.select1 = Genetic.Select1[$scope.select1];

			// selects two individuals for crossover
			genetic.select2 = Genetic.Select2[$scope.select2];

			// function used to create the initial population
			genetic.seed = function () {
				var board = this.userData.clone2DArray(this.userData.initialBoard),
					available = [],
					index,
					boxSize = Math.sqrt(board.length),
					rStart, rEnd, r,
					cStart, cEnd, c,
					i,
					j;

				/* fills in each row with available values
				for (i = 0; i < board.length; i++) {
					for (j = 0; j < board.length; j++) {
						available[j] = j + 1;
					}

					for (j = 0; j < board.length; j++) {
						if (board[i][j].stuck) {
							index = available.indexOf(board[i][j].value);
							if (index !== -1) {
								available.splice(index, 1);
							}
						}
					}

					for (j = 0; j < board.length; j++) {
						if (!board[i][j].stuck) {
							index = this.userData.randRange(0, available.length - 1);
							board[i][j].value = available[index];
							available.splice(index, 1);
						}
					}
				}*/

				/* fills in remaining boxes with available values
				for (i = 0; i < board.length; i++) {
					rStart = Math.floor(i / boxSize) * boxSize;
					rEnd = rStart + boxSize;
					cStart = (i % boxSize) * boxSize;
					cEnd = cStart + boxSize;

					for (j = 0; j < board.length; j++) {
						available[j] = j + 1;
					}

					for (r = rStart; r < rEnd; r++) {
						for (c = cStart; c < cEnd; c++) {
							if (board[r][c].stuck) {
								index = available.indexOf(board[r][c].value);
								if (index !== -1) {
									available.splice(index, 1);
								}
							}
						}
					}

					for (r = rStart; r < rEnd; r++) {
						for (c = cStart; c < cEnd; c++) {
							if (!board[r][c].stuck) {
								index = this.userData.randRange(0, available.length - 1);
								board[r][c].value = available[index];
								available.splice(index, 1);
							}
						}
					}
				}*/

				// fills in non-stuck spaces with random values
				for (i = 0; i < board.length; i++) {
					for (j = 0; j < board.length; j++) {
						if (!board[i][j].stuck) {
							board[i][j].value = this.userData.randRange(1, board.length);
						}
					}
				}

				return board;
			};

			// function to mutate boards
			genetic.mutate = function (board) {
				var length = board.length,
					mutation = this.userData.clone2DArray(board),
					index,
					available = [],
					temp, row, col, i;

				/* get available values
				row = this.userData.randRange(0, length - 1);
				for (i = 0; i < length; i++) {
					if (!mutation[row][i].stuck) {
						available.push(mutation[row][i].value);
					}
				}

				// apply available values randomly
				for (i = 0; i < length; i++) {
					if (!mutation[row][i].stuck) {
						index = this.userData.randRange(0, available.length - 1);
						mutation[row][i].value = available[index];
						available.splice(index, 1);
					}
				}*/

				// single cell random mutation
			 	while (true) {
					row = this.userData.randRange(0, length - 1);
					col = this.userData.randRange(0, length - 1);

					if (!mutation[row][col].stuck) {
						mutation[row][col].value = this.userData.randRange(1, length);
						break;
					}
				}

				return mutation;
			};

			// function to create two new boards from two parent boards
			genetic.crossover = function (boardA, boardB) {
				var length = boardA.length,
					parents = [boardA, boardB],
					boardC = [],
					boardD = [],
					board,
					temp, row, col, i;

				// apply crossover algorithm
				if (this.userData.crossoverAlgorithm === 'Uniform') {
					// create children from random rows from parents
					for (i = 0; i < length; i++) {
						board = this.userData.randRange(0, 1);
						boardC[i] = parents[board][i];

						board = this.userData.randRange(0, 1);
						boardD[i] = parents[board][i];
					}
				} else {
					// create copies of parent boards to be used as children
					boardC = this.userData.clone2DArray(boardA);
					boardD = this.userData.clone2DArray(boardB);

					// randomly swap cells between boards
					for (row = 0; row < length; row++) {
						for (col = 0; col < length; col++) {
							if (!boardC[row][col].stuck) {
								i = Math.random();
								if (i < this.userData.crossover) {
									temp = boardC[row][col].value;
									boardC[row][col].value = boardD[row][col].value;
									boardD[row][col].value = temp;
								}
							}
						}
					}
				}

				// return children
				return [boardC, boardD];
			};

			// function that returns a boards fitness score
			genetic.fitness = function (board) {
				var fitness = 0,
					rowFitness = 0,
					colFitness = 0,
					boxFitness = 0,
					boxSize = Math.sqrt(board.length),
					i,
					j;

				/* fitness is based of correct rows * columns * boxes
				for (i = 0; i < board.length; i++) {
					if (this.userData.checkRow(board, i)) {
						rowFitness++;
					}
					if (this.userData.checkCol(board, i)) {
						colFitness++;
					}
					if (this.userData.checkBox(board, Math.floor(i / boxSize) * boxSize, (i % boxSize) * boxSize)) {
						boxFitness++;
					}
				}*/

				/* fitness is based on incorrect rows, columns and boxes
				for (i = 0; i < board.length; i++) {
					if (!this.userData.checkRow(board, i)) {
						fitness++;
					}
					if (!this.userData.checkCol(board, i)) {
						fitness++;
					}
					if (!this.userData.checkBox(board, Math.floor(i / boxSize) * boxSize, (i % boxSize) * boxSize)) {
						fitness++;
					}
				}*/

				// fitness is based on total correct unstuck cells for each validity test
				for (i = 0; i < board.length; i++) {
					for (j = 0; j < board.length; j++) {
						if (!board[i][j].stuck) {
							value = board[i][j];
							if (this.userData.checkCellRow(board, i, j)) {
								fitness++;
							}
							if (this.userData.checkCellCol(board, i, j)) {
								fitness++;
							}
							if (this.userData.checkCellBox(board, i, j)) {
								fitness++;
							}
						}
					}
				}

				//return rowFitness * colFitness * boxFitness;
				return fitness;
			};

			// called each generation, returns whether the board has been solved
			genetic.generation = function (pop, generation, stats) {
				var board = pop[0].entity,
					boxSize = Math.sqrt(board.length),
					i;

				// check rows, columns and boxes
				for (i = 0; i < board.length; i++) {
					if (!this.userData.checkRow(board, i) ||
					!this.userData.checkCol(board, i) ||
					!this.userData.checkBox(board, Math.floor(i / boxSize) * boxSize, (i % boxSize) * boxSize)) {
						return true;
					}
				}

				return false;
			};

			// used to update the view
			genetic.notification = function (pop, generation, stats, isFinished) {
				// update statistics
				$scope.generation = generation;
				$scope.isFinished = isFinished;
				$scope.board = this.userData.clone2DArray(pop[0].entity);
				$scope.stats = stats;

				// set global maximum
				if (stats.maximum > $scope.globalMax) {
					$scope.globalMax = stats.maximum;
				}

				// add chart data
				$scope.labels.push(generation);
				$scope.data[0].push(stats.maximum);
				$scope.data[1].push(stats.mean);
				$scope.data[2].push(stats.minimum);
				$scope.data[3].push(stats.stdev);

				// update scope
				$scope.$apply();

				// call callback function if finished
				if (isFinished && callback !== undefined) {
					callback(index);
				}
			};

			// start algorithm
			genetic.evolve(config, userData);
		};

		// returns if a cell should be highlighted
		$scope.highlight = function (i, j) {
			var boxSize = Math.sqrt($scope.board.length),
				rowIsEven = Math.ceil(++i / boxSize) % 2 === 0,
				colIsEven = Math.ceil(++j / boxSize) % 2 === 0;

			return (rowIsEven && colIsEven) || (!rowIsEven && !colIsEven);
		};

		// initialization
		init();
	}]);
}());
