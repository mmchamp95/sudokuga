# SudokuGA

### Contributing

The meat of this program is found in js/sudoku-ga.js.

### About

This web application was created for a computer science project in artificial intelligence, specifically genetic algorithms.

This program attempts to solve a randomly generated sudoku puzzle using a genetic algorithm.

### Resources Used

This program makes use of AngularJS, jQuery, Bootstrap, Chart.js, Genetic-JS and Sudoku Generator.
